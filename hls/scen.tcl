# author: Thomas Ogilvie
# sample tcl script showing the use of GPSR and HLS (hierarchical location service)


## GPSR Options
Agent/GPSR set bdesync_                0.5 ;# beacon desync random component
Agent/GPSR set bexp_                   [expr 3*([Agent/GPSR set bint_]+[Agent/GPSR set bdesync_]*[Agent/GPSR set bint_])] ;# beacon timeout interval
Agent/GPSR set pint_                   1.5 ;# peri probe interval
Agent/GPSR set pdesync_                0.5 ;# peri probe desync random component
Agent/GPSR set lpexp_                  8.0 ;# peris unused timeout interval
Agent/GPSR set drop_debug_             1   ;#
Agent/GPSR set peri_proact_            1 	 ;# proactively generate peri probes
Agent/GPSR set use_implicit_beacon_    1   ;# all packets act as beacons; promisc.
Agent/GPSR set use_timed_plnrz_        0   ;# replanarize periodically
Agent/GPSR set use_congestion_control_ 0
Agent/GPSR set use_reactive_beacon_    0   ;# only use reactive beaconing

set val(bint)           0.5  ;# beacon interval
set val(use_mac)        1    ;# use link breakage feedback from MAC
set val(use_peri)       1    ;# probe and use perimeters
set val(use_planar)     1    ;# planarize graph
set val(verbose)        1    ;#
set val(use_beacon)     1    ;# use beacons at all
set val(use_reactive)   0    ;# use reactive beaconing
set val(locs)           0    ;# default to OmniLS
set val(use_loop)       0    ;# look for unexpected loops in peris

set val(agg_mac)          1 ;# Aggregate MAC Traces
set val(agg_rtr)          0 ;# Aggregate RTR Traces
set val(agg_trc)          0 ;# Shorten Trace File

set val(chan)		Channel/WirelessChannel
set val(prop)		Propagation/TwoRayGround
set val(netif)		Phy/WirelessPhy
set val(mac)		Mac/802_11
set val(ifq)		Queue/DropTail/PriQueue
set val(ll)		    LL
set val(ant)		Antenna/OmniAntenna
set val(x)		    1002      ;# X dimension of the topography
set val(y)		    1002      ;# Y dimension of the topography
set val(ifqlen)		512       ;# max packet in ifq
set val(seed)		1.0
set val(adhocRouting)	GPSR      ;# AdHoc Routing Protocol
set val(nn)		    40       ;# how many nodes in dynamic mobility
set val(nall)		42
set val(stop)		240     ;# simulation time
set val(use_gk)		0	  ;# > 0: use GridKeeper with this radius
set val(zip)		0         ;# should trace files be zipped

set val(agttrc)         ON ;# Trace Agent
set val(rtrtrc)         ON ;# Trace Routing Agent
set val(mactrc)         ON ;# Trace MAC Layer
set val(movtrc)         ON ;# Trace Movement


set val(lt)		""
set val(cp)		"map.activity.tcl"
set val(sc)		"map.mobility.tcl"

set val(out)	"gpsr-tracefile-1002.tr"
set val(nam)	"gpsr-tracefile.nam"

Agent/GPSR set locservice_type_ 3

add-packet-header Common Flags IP LL Mac Message GPSR  LOCS SR RTP Ping HLS

Agent/GPSR set bint_                  $val(bint)
# Recalculating bexp_ here
Agent/GPSR set bexp_                 [expr 3*([Agent/GPSR set bint_]+[Agent/GPSR set bdesync_]*[Agent/GPSR set bint_])] ;# beacon timeout interval
Agent/GPSR set use_peri_              $val(use_peri)
Agent/GPSR set use_planar_            $val(use_planar)
Agent/GPSR set use_mac_               $val(use_mac)
Agent/GPSR set use_beacon_            $val(use_beacon)
Agent/GPSR set verbose_               $val(verbose)
Agent/GPSR set use_reactive_beacon_   $val(use_reactive)
Agent/GPSR set use_loop_detect_       $val(use_loop)

CMUTrace set aggregate_mac_           $val(agg_mac)
CMUTrace set aggregate_rtr_           $val(agg_rtr)

# seeding RNG
ns-random $val(seed)

# create simulator instance
set ns_		[new Simulator]

set loadTrace  $val(lt)

set topo	[new Topography]
$topo load_flatgrid $val(x) $val(y)

set tracefd	[open $val(out) w]		;# setting up output files
set nf 		[open $val(nam) w]		;# trace and nam file

$ns_ trace-all $tracefd
$ns_ namtrace-all-wireless $nf $val(x) $val(y) ;# mobily topo x&y

set chanl [new $val(chan)]

# Create God
set god_ [create-god $val(nall)]

# Attach Trace to God
set T [new Trace/Generic]
$T attach $tracefd

#
# Define Nodes
#
puts "Configuring Nodes ($val(nn))"
$ns_ node-config -adhocRouting $val(adhocRouting) \
                 -llType $val(ll) \
                 -macType $val(mac) \
                 -ifqType $val(ifq) \
                 -ifqLen $val(ifqlen) \
                 -antType $val(ant) \
                 -propType $val(prop) \
                 -phyType $val(netif) \
                 -channel $chanl \
		         -topoInstance $topo \
                 -wiredRouting OFF \
         		 -mobileIP OFF \
		         -agentTrace $val(agttrc) \
                 -routerTrace $val(rtrtrc) \
                 -macTrace $val(mactrc) \
                 -movementTrace $val(movtrc)

#
#  Create the specified number of nodes [$val(nn)] and "attach" them
#  to the channel.
for {set i 0} {$i < $val(nn) } {incr i} {
    set node_($i) [$ns_ node]
    $node_($i) random-motion 0		;# disable random motion
    $node_($i) namattach $nf

    # Bring Nodes to God's Attention
    $god_ new_node $node_($i)
}

source $val(sc)
source $val(cp)

# ======================================================================
# Define node initial position in nam
# ======================================================================

for {set i 0} {$i < $val(nn)} {incr i} {
    # 20 defines the node size in nam, must adjust it according to your scenario
    # The function must be called after mobility model is defined
    $ns_ initial_node_pos $node_($i) 20
}

set source_node [$ns_ node]
$source_node set X_ 400.0
$source_node set Y_ 200.00
$source_node set Z_ 0.00
$source_node random-motion 0
$source_node namattach $nf
$ns_ initial_node_pos $source_node 30
$ns_ at 0.0 "$source_node start";
$god_ new_node $source_node

set dest_node [$ns_ node]
$dest_node set X_ 799.25
$dest_node set Y_ 200
$dest_node set Z_ 0.00
$dest_node random-motion 0
$dest_node namattach $nf
$ns_ initial_node_pos $dest_node 30
$ns_ at 0.0 "$dest_node start";
$god_ new_node $dest_node

#Setup UDP connection
set udp_s [new Agent/UDP]
set udp_r [new Agent/Null]
$ns_ attach-agent $source_node $udp_s
$ns_ attach-agent $dest_node $udp_r


#Setup a MM Application
set e [new Application/Traffic/CBR]
$e attach-agent $udp_s
$e set packetSize_ 512
$e set rate_ 2kB
$e set maxpkts_ 20000
$e set random_ 1
$ns_ connect $udp_s $udp_r

#Simulation Scenario
$ns_ at 40.0 "$e start"

#
# Tell nodes when the simulation ends
#
for {set i 0} {$i < $val(nn) } {incr i} {
    $ns_ at $val(stop).0 "$node_($i) reset";
}

$ns_ at $val(stop).0 "$source_node reset";
$ns_ at $val(stop).0 "$dest_node reset";

$ns_ at  $val(stop).0002 "puts \"NS EXITING... $val(out)\" ; $ns_ halt"
proc stop {} {
   	global ns_ tracefd
	global ns_ nf
    	$ns_ flush-trace
	close $nf
    	close $tracefd
	exit 0
}

puts "Starting Simulation..."
$ns_ run
