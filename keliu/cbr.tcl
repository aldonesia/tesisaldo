
# GPSR routing agent settings
for {set i 0} {$i < $opt(nn)} {incr i} {
    $ns_ at 0.00002 "$ragent_($i) turnon"
    $ns_ at 3.0 "$ragent_($i) neighborlist"
    # $ns_ at 3.0 "$ragent_($i) startSink 10.0"
    # $ns_ at 20.0 "$ragent_($i) neighborlist"
#    $ns_ at 30.0 "$ragent_($i) turnoff"
}

$ns_ at 3.0 "$ragent_(74) startSink 10.0"
# $ns_ at 11.0 "$ragent_(0) startSink 10.0"
# $ns_ at 10.5 "$ragent_(99) startSink 10.0"


# GPSR routing agent dumps
# $ns_ at 25.0 "$ragent_(74) sinklist"


# Upper layer agents/applications behavior
set udp_(0) [new Agent/UDP]
$ns_ attach-agent $node_(74) $udp_(0)
set null_(0) [new Agent/Null]
$ns_ attach-agent $node_(99) $null_(0)

set cbr_(0) [new Application/Traffic/CBR]
$cbr_(0) set packetSize_ 32
$cbr_(0) set interval_ 1.0
$cbr_(0) set random_ 1
$cbr_(0) set maxpkts_ 10000
$cbr_(0) attach-agent $udp_(0)
$ns_ connect $udp_(0) $null_(0)
# $ns_ at 22.5568388786897245 "$cbr_(0) start"
$ns_ at 10.0 "$cbr_(0) start"
$ns_ at 195.0 "$cbr_(0) stop"
